```julia

Pkg.add("Plots")
Pkg.add("SpatialDependence")
Pkg.add("SpatialDatasets")
Pkg.add("StableRNGs")

using SpatialDependence
using SpatialDatasets
using StableRNGs
```


```julia
# Load packages
using Plots
using SpatialDependence
using SpatialDatasets
using StableRNGs

# Guerry's Moral statistics of France data from the SpatialDatasets.jl package
guerry = sdataset("Guerry");

# Plot Litercy variable
plot(guerry, :Litercy, NaturalBreaks(), legend = :topleft, title = "Litercy")
```

    [32m[1m Downloading[22m[39m artifact: GeoDaGuerry
    




    
![svg](output_1_1.svg)
    




```julia
# Build polygon contiguity matrix
W = polyneigh(guerry.geometry);
W
```




    Spatial Weights 
    Observations: 85 
    Transformation: row
    Minimum nunmber of neighbors: 2
    Maximum nunmber of neighbors: 8
    Average number of neighbors: 4.9412
    Median number of neighbors: 5.0
    Islands (isloated): 0
    Density: 5.8131% 
    




```julia
# Global Moran test of Spatial Autocorrelation of the Litercy variable
moran(guerry.Litercy, W, permutations = 9999, rng = StableRNG(1234567))
```




    Moran's I test of Global Spatial Autocorrelation
    --------------------------------------------
    
    Moran's I: 0.7176053
    Expectation: -0.0119048
    
    Randomization test with 9999 permutations.
     Mean: -0.0125941
     Std Error: 0.0707896
     zscore: 10.3150637
     p-value: 0.0001
    




```julia
# Moran Scatterplot of the Litercy variable
plot(guerry.Litercy, W, xlabel = "Litercy")
```




    
![svg](output_4_0.svg)
    




```julia
# Local Indicators of Spatial Association (LISA) - Local Moran
lmguerry = localmoran(guerry.Litercy, W, permutations = 9999, rng = StableRNG(1234567))
```




    Local Moran test of Spatial Autocorrelation
    --------------------------------------------
    
    Randomization test with 9999 permutations.
    Interesting locations at 0.05 significance level:
           High-High: 18
             Low-Low: 20
            Low-High: 0
            High-Low: 0
    




```julia
# LISA Cluster Map
plot(guerry, lmguerry, sig = 0.05, adjust = :fdr)
```




    
![svg](output_6_0.svg)
    




```julia

```
