# Material suplementario del artículo: PRIMER REGISTRO DEL GÉNERO DE DINOFLAGELADO TECADO METADINOPHYSIS NIE & WANG (DINOPHYCEAE: DINOPHYSIALES) Y ANÁLISIS DE SU ABUNDANCIA EN ÁREAS COSTERAS DEL PACIFICO COLOMBIANO.

Este es el analisis pepito perez


```R
install.packages("ape")
install.packages("gstat")
install.packages("tidyverse")
library(ape)
library(gstat)
library(tidyverse)
```


```R
sessionInfo()
```


    R version 4.1.3 (2022-03-10)
    Platform: x86_64-w64-mingw32/x64 (64-bit)
    Running under: Windows 10 x64 (build 19041)
    
    Matrix products: default
    
    locale:
    [1] LC_COLLATE=Spanish_Spain.1252  LC_CTYPE=Spanish_Spain.1252   
    [3] LC_MONETARY=Spanish_Spain.1252 LC_NUMERIC=C                  
    [5] LC_TIME=Spanish_Spain.1252    
    
    attached base packages:
    [1] stats     graphics  grDevices utils     datasets  methods   base     
    
    loaded via a namespace (and not attached):
     [1] fansi_1.0.3     utf8_1.2.2      digest_0.6.29   crayon_1.5.1   
     [5] IRdisplay_1.1   repr_1.1.4      lifecycle_1.0.1 jsonlite_1.8.0 
     [9] evaluate_0.15   pillar_1.7.0    rlang_1.0.3     cli_3.3.0      
    [13] uuid_1.1-0      vctrs_0.4.1     ellipsis_0.3.2  IRkernel_1.3   
    [17] tools_4.1.3     glue_1.6.2      fastmap_1.1.0   compiler_4.1.3 
    [21] base64enc_0.1-3 pbdZMQ_0.3-7    htmltools_0.5.2



```R


datosAbundancia<-read.csv(url("https://gitlab.com/ChrisBermudezR/metadinophysis/-/raw/main/Analisis/datosAbundancia.csv"))
datosAbundancia
```


<table class="dataframe">
<caption>A data.frame: 28 × 6</caption>
<thead>
	<tr><th scope=col>Longitud</th><th scope=col>Latitud</th><th scope=col>Estaciones</th><th scope=col>Marea</th><th scope=col>Bahia</th><th scope=col>Abundancia</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>-77.32536</td><td>3.97272</td><td>M1B</td><td>Baja</td><td>Malaga      </td><td>  0</td></tr>
	<tr><td>-77.32061</td><td>3.89664</td><td>M2B</td><td>Baja</td><td>Malaga      </td><td>100</td></tr>
	<tr><td>-77.35525</td><td>3.90114</td><td>M3B</td><td>Baja</td><td>Malaga      </td><td>  0</td></tr>
	<tr><td>-77.32536</td><td>3.92775</td><td>M4B</td><td>Baja</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.33739</td><td>3.93908</td><td>M5B</td><td>Baja</td><td>Malaga      </td><td> 20</td></tr>
	<tr><td>-77.27608</td><td>3.99153</td><td>M6B</td><td>Baja</td><td>Malaga      </td><td>160</td></tr>
	<tr><td>-77.06667</td><td>3.86667</td><td>B1B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.06750</td><td>3.89417</td><td>B2B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.08167</td><td>3.89200</td><td>B3B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.11167</td><td>3.86000</td><td>B4B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.13200</td><td>3.84917</td><td>B5B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.14550</td><td>3.84083</td><td>B6B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.14033</td><td>3.82083</td><td>B7B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.06194</td><td>3.88972</td><td>B8B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.32536</td><td>3.97272</td><td>M1A</td><td>Alta</td><td>Malaga      </td><td>140</td></tr>
	<tr><td>-77.32061</td><td>3.89664</td><td>M2A</td><td>Alta</td><td>Malaga      </td><td>140</td></tr>
	<tr><td>-77.35525</td><td>3.90114</td><td>M3A</td><td>Alta</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.32536</td><td>3.92775</td><td>M4A</td><td>Alta</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.33739</td><td>3.93908</td><td>M5A</td><td>Alta</td><td>Malaga      </td><td> 80</td></tr>
	<tr><td>-77.27608</td><td>3.99153</td><td>M6A</td><td>Alta</td><td>Malaga      </td><td>200</td></tr>
	<tr><td>-77.06667</td><td>3.86667</td><td>B1A</td><td>Alta</td><td>Buenaventura</td><td>240</td></tr>
	<tr><td>-77.06750</td><td>3.89417</td><td>B2A</td><td>Alta</td><td>Buenaventura</td><td>300</td></tr>
	<tr><td>-77.08167</td><td>3.89200</td><td>B3A</td><td>Alta</td><td>Buenaventura</td><td> 40</td></tr>
	<tr><td>-77.11167</td><td>3.86000</td><td>B4A</td><td>Alta</td><td>Buenaventura</td><td> 40</td></tr>
	<tr><td>-77.13200</td><td>3.84917</td><td>B5A</td><td>Alta</td><td>Buenaventura</td><td>860</td></tr>
	<tr><td>-77.14550</td><td>3.84083</td><td>B6A</td><td>Alta</td><td>Buenaventura</td><td>140</td></tr>
	<tr><td>-77.14033</td><td>3.82083</td><td>B7A</td><td>Alta</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.06194</td><td>3.88972</td><td>B8A</td><td>Alta</td><td>Buenaventura</td><td>340</td></tr>
</tbody>
</table>




```R
wilcox.test(datosAbundancia$Abundancia~datosAbundancia$Marea)
```

    Warning message in wilcox.test.default(x = c(140L, 140L, 60L, 60L, 80L, 200L, 240L, :
    "cannot compute exact p-value with ties"
    


    
    	Wilcoxon rank sum test with continuity correction
    
    data:  datosAbundancia$Abundancia by datosAbundancia$Marea
    W = 175, p-value = 0.0003757
    alternative hypothesis: true location shift is not equal to 0
    



```R
wilcox.test(datosAbundancia$Abundancia~datosAbundancia$Bahia)
```

    Warning message in wilcox.test.default(x = c(0L, 20L, 0L, 20L, 20L, 0L, 0L, 0L, :
    "cannot compute exact p-value with ties"
    


    
    	Wilcoxon rank sum test with continuity correction
    
    data:  datosAbundancia$Abundancia by datosAbundancia$Bahia
    W = 78, p-value = 0.4111
    alternative hypothesis: true location shift is not equal to 0
    



```R

```


```R
SoloBaja<-datosAbundancia %>% dplyr:::filter.data.frame( Marea=="Baja")
SoloBaja
SoloAlta<-datosAbundancia %>% dplyr:::filter.data.frame( Marea=="Alta")
SoloAlta
```


<table class="dataframe">
<caption>A data.frame: 14 × 6</caption>
<thead>
	<tr><th scope=col>Longitud</th><th scope=col>Latitud</th><th scope=col>Estaciones</th><th scope=col>Marea</th><th scope=col>Bahia</th><th scope=col>Abundancia</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>-77.32536</td><td>3.97272</td><td>M1B</td><td>Baja</td><td>Malaga      </td><td>  0</td></tr>
	<tr><td>-77.32061</td><td>3.89664</td><td>M2B</td><td>Baja</td><td>Malaga      </td><td>100</td></tr>
	<tr><td>-77.35525</td><td>3.90114</td><td>M3B</td><td>Baja</td><td>Malaga      </td><td>  0</td></tr>
	<tr><td>-77.32536</td><td>3.92775</td><td>M4B</td><td>Baja</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.33739</td><td>3.93908</td><td>M5B</td><td>Baja</td><td>Malaga      </td><td> 20</td></tr>
	<tr><td>-77.27608</td><td>3.99153</td><td>M6B</td><td>Baja</td><td>Malaga      </td><td>160</td></tr>
	<tr><td>-77.06667</td><td>3.86667</td><td>B1B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.06750</td><td>3.89417</td><td>B2B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.08167</td><td>3.89200</td><td>B3B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.11167</td><td>3.86000</td><td>B4B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.13200</td><td>3.84917</td><td>B5B</td><td>Baja</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.14550</td><td>3.84083</td><td>B6B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.14033</td><td>3.82083</td><td>B7B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
	<tr><td>-77.06194</td><td>3.88972</td><td>B8B</td><td>Baja</td><td>Buenaventura</td><td>  0</td></tr>
</tbody>
</table>




<table class="dataframe">
<caption>A data.frame: 14 × 6</caption>
<thead>
	<tr><th scope=col>Longitud</th><th scope=col>Latitud</th><th scope=col>Estaciones</th><th scope=col>Marea</th><th scope=col>Bahia</th><th scope=col>Abundancia</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>-77.32536</td><td>3.97272</td><td>M1A</td><td>Alta</td><td>Malaga      </td><td>140</td></tr>
	<tr><td>-77.32061</td><td>3.89664</td><td>M2A</td><td>Alta</td><td>Malaga      </td><td>140</td></tr>
	<tr><td>-77.35525</td><td>3.90114</td><td>M3A</td><td>Alta</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.32536</td><td>3.92775</td><td>M4A</td><td>Alta</td><td>Malaga      </td><td> 60</td></tr>
	<tr><td>-77.33739</td><td>3.93908</td><td>M5A</td><td>Alta</td><td>Malaga      </td><td> 80</td></tr>
	<tr><td>-77.27608</td><td>3.99153</td><td>M6A</td><td>Alta</td><td>Malaga      </td><td>200</td></tr>
	<tr><td>-77.06667</td><td>3.86667</td><td>B1A</td><td>Alta</td><td>Buenaventura</td><td>240</td></tr>
	<tr><td>-77.06750</td><td>3.89417</td><td>B2A</td><td>Alta</td><td>Buenaventura</td><td>300</td></tr>
	<tr><td>-77.08167</td><td>3.89200</td><td>B3A</td><td>Alta</td><td>Buenaventura</td><td> 40</td></tr>
	<tr><td>-77.11167</td><td>3.86000</td><td>B4A</td><td>Alta</td><td>Buenaventura</td><td> 40</td></tr>
	<tr><td>-77.13200</td><td>3.84917</td><td>B5A</td><td>Alta</td><td>Buenaventura</td><td>860</td></tr>
	<tr><td>-77.14550</td><td>3.84083</td><td>B6A</td><td>Alta</td><td>Buenaventura</td><td>140</td></tr>
	<tr><td>-77.14033</td><td>3.82083</td><td>B7A</td><td>Alta</td><td>Buenaventura</td><td> 20</td></tr>
	<tr><td>-77.06194</td><td>3.88972</td><td>B8A</td><td>Alta</td><td>Buenaventura</td><td>340</td></tr>
</tbody>
</table>




```R
geo_dis<-as.matrix(dist(cbind(SoloBaja$Longitud, SoloBaja$Latitud)))
geo_Inv<-1/geo_dis
diag(geo_Inv)<-0
```


```R
#Calculo de Indice de Moran

ape::Moran.I(SoloBaja$Abundancia, geo_Inv, na.rm =TRUE)

ape::Moran.I(SoloAlta$Abundancia, geo_Inv, na.rm =TRUE)


```


<dl>
	<dt>$observed</dt>
		<dd>0.0132649254190303</dd>
	<dt>$expected</dt>
		<dd>-0.0769230769230769</dd>
	<dt>$sd</dt>
		<dd>0.0927041841349853</dd>
	<dt>$p.value</dt>
		<dd>0.330623907520184</dd>
</dl>




<dl>
	<dt>$observed</dt>
		<dd>-0.10355785610815</dd>
	<dt>$expected</dt>
		<dd>-0.0769230769230769</dd>
	<dt>$sd</dt>
		<dd>0.0760891754835862</dd>
	<dt>$p.value</dt>
		<dd>0.726303508446382</dd>
</dl>


